#!/bin/bash

. ./other.sh

while true
do
	collect_metrics
	generate_prometheus_html
	serve_metrics_page
	sleep 3
done
