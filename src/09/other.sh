#!/bin/bash

function collect_metrics {
	system_cpu_usage=$(top -bn1 | grep "Cpu(s)" | awk '{print $2}' | awk -F. '{print $1}' | tr ',' '.')
	ram_total=$(free | awk 'FNR == 2 {print $2}')
	ram_used=$(free | awk 'FNR == 2 {print $3}')
	system_ram_usage=$((ram_used * 100 / ram_total))
	system_disk_usage=$(df -h / | awk 'NR == 2 {print $5}' | cut -d '%' -f1)
}

function generate_prometheus_html {
	cat <<EOF >metrics.html
# HELP system_cpu_usage The CPU usage percentage
# TYPE system_cpu_usage gauge
system_cpu_usage $system_cpu_usage

# HELP system_ram_usage The RAM usage percentage
# TYPE system_ram_usage gauge
system_ram_usage $system_ram_usage

# HELP system_disk_usage The disc usage of the root filesystem percentage
# TYPE system_disk_usage gauge
system_disk_usage $system_disk_usage
EOF
}

function serve_metrics_page {
	sudo cp metrics.html /var/www/html/metrics.html
}
