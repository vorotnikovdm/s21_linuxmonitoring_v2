#!/bin/bash

. ./other.sh

if [ "$#" -ne 6 ]; then
	echo "Usage: $0 <path> <num_subfolders> <folder_chars> <num_files> <file_chars> <file_size_kb>"
	elif ! [[ $2 =~ ^[0-9]+$ ]]; then
		echo "Error: <num_subfolders> should be a positive number"
	elif [ ${#3} -gt 7 ]; then
		echo "Error: <folder_chars> has more than 7 characters"
	elif ! [[ $4 =~ ^[0-9]+$ ]]; then
		echo "Error: <num_files> should be a positive number"
	else
		file_chars_check "$5"
		if [[ $? -eq 1 ]]; then
			echo "Error: <file_chars> should be in format <max7chars.max3chars>"
		else
			file_size_check "$6"
			dir_create "$1"
			generate_subfolders_and_files "$1" "$2" "$3" "$4" "$5" "$6"
		fi
fi
