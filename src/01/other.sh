#!/bin/bash

function dir_create {
	mkdir -p "$1"
}

function generate_random_string {
	local length="$1"
	local characters="$2"
	echo "$(LC_CTYPE=c tr -dc "$characters" < /dev/urandom | head -c "$length")"
}

function space_check {
	local avail_space=$(df --output=avail / | tail -1)
	local min_space=$((1024 * 1024))
	if [[ $avail_space -lt $min_space ]]; then
		echo "Error: not enough space"
		exit 1
	fi
}

function generate_subfolders_and_files {
	local path="$1"
	local num_subfolders="$2"
	local folder_chars="$3"
	local num_files="$4"
	local file_chars="$5"
	local file_chars_name=$(echo "$file_chars" | cut -d'.' -f1)
	local file_chars_extension=$(echo "$file_chars" | cut -d'.' -f2)
	local size_kb="$6"
	local file_size_kb="${size_kb%%[^0-9]*}"
	local time_temp=$(date +"%d%m%y")
	local log_file="logs.log"
	for ((i = 1; i <= num_subfolders; i++)); do
		folder_name=$(generate_random_string 7 "$folder_chars")
		folder_path="$path/$folder_name""_""$time_temp"
		mkdir -p "$folder_path"
		echo "Path: ${folder_path} Date: $(date '+%Y-%m-%d %H:%M:%S')" >> "$log_file"
		for ((j = 1; j <= num_files; j++)); do
			space_check
			file_name=$(generate_random_string 7 "$file_chars_name")
			file_extension=$(generate_random_string 3 "$file_chars_extension")
			file="$folder_path/$file_name""_""$time_temp"".$file_extension"
			dd if=/dev/zero of="$file" bs=1024 count="$file_size_kb" status=none
			echo "Path: ${file} Date: $(date '+%Y-%m-%d %H:%M:%S') Size: ${size_kb}" >> "$log_file"

		done
	done
}

function file_chars_check {
	local file_chars="$1"
	local file_chars_name=$(echo "$file_chars" | cut -d'.' -f1)
	local file_chars_extension=$(echo "$file_chars" | cut -d'.' -f2)
	if [ ${#file_chars_name} -gt 7 ] || [ ${#file_chars_extension} -gt 3 ] || ! [[ "$file_chars" == *.* ]]; then
		return 1
	else
		return 0
	fi
}

function file_size_check {
	local size_kb="$1"
	local file_size_kb="${size_kb%%[^0-9]*}"
	local kb_part="${size_kb##*[0-9]}"
	if ! [[ ${file_size_kb} =~ ^[0-9]+$ ]] || [ ${file_size_kb} -gt 100 ] || [ "$kb_part" != "kb" ]; then
		echo "Error: <file_size_kb> should be in format <max100kb>"
		exit 1
	fi
}
