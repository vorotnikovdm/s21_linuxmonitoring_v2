#!/bin/bash

function error_output {
	echo "Usage: $0 <method>"
	echo "Methods:"
	echo "1 - sort by code"
	echo "2 - sort by unique ip"
	echo "3 - sort by error request"
	echo "4 - sort by unique ip + error request"
}

function method_usage {
	if [ "$1" -eq 1 ]; then
		sort_by_response_code "$2"
	elif [ "$1" -eq 2 ]; then
		sort_by_unique_ip "$2"
	elif [ "$1" -eq 3 ]; then
		sort_by_error_request "$2"
	elif [ "$1" -eq 4 ]; then
		sort_by_error_and_ip "$2"
	fi	
}

function sort_by_response_code {
	awk '{print $9, $0}' "$1" | sort -n | cut -d ' ' -f2-
}

function sort_by_unique_ip {
	awk '{print $1}' "$1" | sort -u	
}

function sort_by_error_request {
	awk '$9 ~ /4[0-9][0-9]|5[0-9][0-9]/{print $0}' "$1"	
}

function sort_by_error_and_ip {
	awk '$9 ~ /4[0-9][0-9]|5[0-9][0-9]/{print $0}' "$1" | sort -u	
}
