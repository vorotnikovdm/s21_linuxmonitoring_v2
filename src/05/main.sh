#!/bin/bash

. ./other.sh

if [ "$#" -ne 1 ] || ! [[ $1 =~ ^[1-4]+$ ]]; then
	error_output
else
	log_file="../04/nginx_log_file_1.log"
	method_usage "$1" "$log_file"
fi
