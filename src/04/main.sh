#!/bin/bash

. ./other.sh

if [ "$#" -ne 0 ]; then
	echo "Error: usage $0"
else
	declare -a agents=("Mozilla" "Google Chrome" "Opera" "Safari" "Internet Explorer" "Microsoft Edge" "Crawler and bot" "Library and net tool")
	declare -a methods=("GET" "POST" "PUT" "PATCH" "DELETE")
	declare -a response_codes=("200" "201" "400" "401" "403" "404" "500" "501" "502" "503")
	for i in {1..5}; do
		log_file="nginx_log_file_${i}.log"
		touch "$log_file"
		num_entries=$(($RANDOM % 901 + 100))
		for ((j = 0; j < num_entries; j++)); do
			ip=$(generate_ip)
			date="$(generate_date)"
			method=$(generate_method)
			response_code=$(generate_response_code)
			url=$(generate_url)
			agent=$(generate_agent)
			echo "$ip - - [$date] \"$method $url HTTP/1.1\" $response_code 0 \"-\" \"$agent\"" >> "$log_file"
		done
	done
fi

#Response codes:
#200 - NGX_HTTP_OK
#201 - NGX_HTTP_CREATED
#400 - NGX_HTTP_BAD_REQUEST
#401 - NGX_HTTP_UNAUTHORIZED
#403 - NGX_HTTP_FORBIDDEN
#404 - NGX_HTTP_NOT_FOUND
#500 - NGX_HTTP_INTERNAL_SERVER_ERROR
#501 - NGX_HTTP_NOT_IMPLEMENTED
#502 - NGX_HTTP_BAD_GATEWAY
#503 - NGX_HTTP_SERVICE_UNAVAILABLE
