#!/bin/bash

function generate_ip {
	echo "$(($RANDOM % 256)).$(($RANDOM % 256)).$(($RANDOM % 256)).$(($RANDOM % 256))"
}

function generate_date {
	local date=$(date +'%d/%b/%Y:%H:%M:%S %z')
	echo "$date"
}

function generate_agent {
	echo "${agents[$(($RANDOM % ${#agents[@]}))]}"
}

function generate_method {
	echo "${methods[$(($RANDOM % ${#methods[@]}))]}"
}

function generate_response_code {
	echo "${response_codes[$(($RANDOM % ${#response_codes[@]}))]}"
}

function generate_url {
	echo "http://www.example.com/${RANDOM}"
}

