#!/bin/bash

function dir_create {
	mkdir -p "$1"
}

function generate_random_string {
	local length="$1"
	local characters="$2"
	echo "$(LC_CTYPE=c tr -dc "$characters" < /dev/urandom | head -c "$length")"
}

function space_check {
	local avail_space=$(df --output=avail / | tail -1)
	local min_space=$((1024 * 1024))
	if [[ $avail_space -lt $min_space ]]; then
		return 1
	fi
	return 0
}

function generate_subfolders_and_files {
	local folder_chars="$1"
	local file_chars="$2"
	local file_size="$3"
	local file_chars_name=$(echo "$file_chars" | cut -d'.' -f1)
	local file_chars_extension=$(echo "$file_chars" | cut -d'.' -f2)
	local file_size_mb="${file_size%%[^0-9]*}"
	local path="$(pwd)"
	local time_temp=$(date +"%d%m%y")
	local log_file="logs.log"
	local max_folders=100
	local flag_temp=0
	for ((i = 0; i < max_folders; i++)); do
		if [[ $flag_temp -eq 1 ]]; then
			break
		fi
		folder_name=$(generate_random_string 7 "$folder_chars")
		folder_path="$path/$folder_name""_""$time_temp"
		mkdir -p "$folder_path"
		echo "Path: ${folder_path} Date: $(date '+%Y-%m-%d %H:%M:%S')" >> "$log_file"
		num_files=$((RANDOM % 100 + 1))
		for ((j = 0; j < num_files; j++)); do
			space_check
			if [[ $? -eq 1 ]]; then
				flag_temp=1
				break
			else
				file_name=$(generate_random_string 7 "$file_chars_name")
				file_extension=$(generate_random_string 3 "$file_chars_extension")
				file="$folder_path/$file_name""_""$time_temp"".$file_extension"
				dd if=/dev/zero of="$file" bs=1M count="$file_size_mb" status=none
				echo "Path: ${file} Date: $(date '+%Y-%m_%d %H:%M:%S') Size: ${file_size}" >> "$log_file"
			fi
		done
	done
}

function folder_chars_check {
	local folder_chars="$1"
	if [ ${#folder_chars} -gt 7 ]; then
		echo "Error: <folder_chars> should be in format <max7chars>"
		exit 1
	fi
}

function file_chars_check {
	local file_chars="$1"
	local file_chars_name=$(echo "$file_chars" | cut -d'.' -f1)
	local file_chars_extension=$(echo "$file_chars" | cut -d'.' -f2)
	if [ ${#file_chars_name} -gt 7 ] || [ ${#file_chars_extension} -gt 3 ] || ! [[ "$file_chars" == *.* ]]; then
		echo "Error: <file_chars> should be in format <max7chars.max3chars>"
		exit 1
	fi
}

function file_size_check {
	local size_mb="$1"
	local file_size_mb="${size_mb%%[^0-9]*}"
	local mb_part="${size_mb##*[0-9]}"
	if ! [[ $file_size_mb =~ ^[0-9]+$ ]] || [ ${file_size_mb} -gt 100 ] || [ "$mb_part" != "Mb" ]; then
		echo "Error: <file_size_mb> should be in format <max100Mb>"
		exit 1
	fi
}
