#!/bin/bash

temp=$(pwd)
temp_path=$(echo "$temp" | awk -F 'src' '{print $1}')
temp_path=$(echo "$temp_path")src/02/other.sh

. $temp_path

start_time=$(date +%s.%N)
temp_start_time=$(date +%H:%M:%S)
if [ "$#" -ne 3 ]; then
	echo "Usage: $0 <folder_chars> <file_chars> <file_size_Mb>"
else
	folder_chars_check "$1"
	file_chars_check "$2"
	file_size_check "$3"
	generate_subfolders_and_files "$1" "$2" "$3"
	end_time=$(date +%s.%N)
	execution_time=$(echo "$end_time - $start_time" | bc)
	temp_end_time=$(date +%H:%M:%S)
	echo "Start time: $temp_start_time End time: $temp_end_time Duration: $execution_time"
	echo "Start time: $temp_start_time End time: $temp_end_time Duration: $execution_time" >> logs.log
fi	
