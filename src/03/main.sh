#!/bin/bash

. ./other.sh

if [ "$#" -ne 1 ] || ! [[ $1 =~ ^[1-3]+$ ]]; then
	error_output
else
	clear_option "$1"
fi

