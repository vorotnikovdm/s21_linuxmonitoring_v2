#!/bin/bash

function error_output {
	echo "Usage: $0 <method>"
	echo "Methods:"
	echo "1 - Clear by log file"
	echo "2 - Clear by creation date and time"
	echo "3 - Clear by name mask"
}

function clear_option {
	if [ "$1" -eq 1 ]; then
		clear_by_log_file
	elif [ "$1" -eq 2 ]; then
		clear_by_date
	elif [ "$1" -eq 3 ]; then
		clear_by_mask
	fi
}

function deleting {
	if [ -f "$1" ]; then
		rm -f "$1" 2>/dev/null
	elif [ -d "$1" ]; then
		rm -rf "$1" 2>/dev/null
	fi
}

function clear_by_log_file {
	read -p "Enter path of log file: " log_path
	declare -a paths
	if [ -f "$log_path" ]; then
	while read -r line; do
		path=$(echo "$line" | awk '{printf $2}')
		if [[ "$line" == *Path:* ]]; then
		paths+=("$path")
		fi
	done < "$log_path"
	else
		echo "log_path is not valid"
		exit 1
	fi
	for path_del in "${paths[@]}"; do
		deleting "$path_del"
	done
}

function clear_by_date {
	read -p "Enter start time in format <YYYY-MM-DD HH:MM> " start_time
	date_check "$start_time"
	if [ "$?" -eq 0 ]; then
		read -p "Enter end time in format <YYYY-MM-DD HH:MM> " end_time
		date_check "$end_time"
		if [ "$?" -eq 0 ]; then
			local start_time_temp=$(date -d "$start_time" +%s)
			local end_time_temp=$(date -d "$end_time" +%s)
			while read -r line; do
				time_temp=("${line%%|*}")
				path_temp=("${line#*|}")
				if [[ "$time_temp" -gt "$start_time_temp" ]] && [[ "$time_temp" -lt "$end_time_temp" ]]; then
					deleting_check "$path_temp"
					if [ "$?" -eq 0 ]; then
						#echo "$time_temp, $path_temp" >> logs.log
						deleting "$path_temp"
					fi
				fi
			done < <(find / -not \( -path /bin -prune -o -path /sbin -prune \) -type f,d -printf "%Ts|%p\n" 2>/dev/null)
		fi
	fi
}

function deleting_check {
	temp=$(pwd)
	temp_check=$(echo "$temp" | awk -F 'src' '{print $1}')
	if [[ "$1" =~ ^"$temp_check"src/[0-9]{2}$ ]] || [[ "$1" =~ ^"$temp_check"src/[0-9]{2}/main.sh$ ]] || [[ "$1" =~ ^"$temp_check"src/[0-9]{2}/other.sh$ ]]; then
		return 1
	else
		return 0
	fi
}

function date_check {
	local pattern="^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}$"
	if ! [[ "$1" =~ $pattern ]]; then
		echo "Not valid date or time, use format <YYYY-MM-DD HH:MM>"
		return 1
	fi
	if ! date -d "$1" &> /dev/null; then
		echo "Not valid date or time, use format <YYYY-MM-DD HH:MM>"
		return 1
	fi
	return 0
}

function clear_by_mask {
	read -p "Enter name mask: " name_mask
	while read -r path_temp line; do
		#echo "$path_temp" >> logs.log
		deleting "$path_temp"
	done < <(find / -name "*$name_mask*" -type d,f -not \( -path /bin -prune -o -path /sbin -prune \) 2>/dev/null)
}
