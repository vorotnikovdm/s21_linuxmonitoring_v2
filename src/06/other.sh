#!/bin/bash

function error_output {
	echo "Usage: $1"
}

function goaccess_log {
	goaccess ../04/nginx_log_file_1.log --log-format=COMBINED -o report.html
	xdg-open report.html
}
